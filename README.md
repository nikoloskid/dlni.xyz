# Personal Website


> Daniel Nikoloski
>
> Based on a jekyll [hacker theme](https://github.com/pages-themes/hacker)

## Prerequisite

- `docker`
- `docker-compose`

## Local development

`docker-compose -f docker-compose-dev.yml up --build; docker-compose -f docker-compose-dev.yml down`

## Production
Deployment via CI/CD 
