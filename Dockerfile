# Base image for Jekyll build
FROM jekyll/jekyll as jekyllbuild

# Set working directory
WORKDIR /srv/jekyll

# Copy source code
COPY . .

# Build the Jekyll site
RUN jekyll build


# Base image for Nginx
FROM nginx:1.25.0-alpine

# Copy built Jekyll site to the nginx webroot
COPY --from=jekyllbuild /srv/jekyll/_site /var/www/public

# Copy nginx configuration file
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

# Copy custom 404 page
COPY ./nginx/html/404.html /usr/share/nginx/html/404.html

# Expose port
EXPOSE 80
